/*******************************************************************************
* File Name: main.h
*
* Version 1.0
*
* Description:
*  Contains the function prototypes and constants available to the example
*  project.
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(MAIN_H)
#define MAIN_H
    
#include <project.h>
#include <stdio.h>

#include "debug.h"

/* Profile specific includes */

#include "FreeRTOS.h"
#include "task.h"
#include "attitude.h"
#include "tstate.h"
#include "ble.h"
#include "hrss.h"
#include "drivers.h"
#include "mqx_tasks.h"
#include "pedo.h"
    
#define LED_ON                      (1u)
#define LED_OFF                     (0u)
    
#define PASSKEY                     (0x04u)

#define WDT_COUNTER                                   (CY_SYS_WDT_COUNTER1)
#define WDT_COUNTER_MASK                              (CY_SYS_WDT_COUNTER1_MASK)
#define WDT_INTERRUPT_SOURCE                          (CY_SYS_WDT_COUNTER1_INT) 
#define WDT_COUNTER_ENABLE                            (1u)
#define WDT_TIMEOUT                                   (32u) /* 1ms @ 32.768kHz clock */
    
/***************************************
*      API Function Prototypes
***************************************/
void StartAdvertisement(void);


/***************************************
*      External data references
***************************************/
extern CYBLE_API_RESULT_T apiResult;
extern uint16 i;
extern uint8 flag;
    
#endif /* MAIN_H */

/* [] END OF FILE */
