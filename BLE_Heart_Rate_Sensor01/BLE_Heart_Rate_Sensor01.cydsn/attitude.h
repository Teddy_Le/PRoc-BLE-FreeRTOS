/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */


#ifndef ATTITUDE_H
#define ATTITUDE_H
    
#include "FreeRTOS.h"\
    
void vStartReadDataFlashTasks( UBaseType_t uxPriority );
void vStartFusionFlashTasks( UBaseType_t uxPriority );
void vStartMagCalFlashTasks( UBaseType_t uxPriority );
   
    
#endif