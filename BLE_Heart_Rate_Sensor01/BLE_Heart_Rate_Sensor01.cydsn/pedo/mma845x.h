
#ifndef MMA845X_H_
#define MMA845X_H_

#include <project.h>
    
extern volatile uint8 I2C_Status;

#define I2C_SENT_FLAG       0x01
#define I2C_RCVD_FLAG       0x02

#define FXOS8700_I2C_ADDR				0x1E
/***********************************************************************************************
**
**#define  MMA845xQ Sensor Internal Registers
*/
#define  MMA845x_STATUS				0x00	//F_STATUS for MMA8451 only
#define  MMA845x_OUT_X_MSB			0x01
#define  MMA845x_OUT_X_LSB			0x02
#define  MMA845x_OUT_Y_MSB			0x03
#define  MMA845x_OUT_Y_LSB			0x04
#define  MMA845x_OUT_Z_MSB			0x05
#define  MMA845x_OUT_Z_LSB			0x06
#define  MMA845x_F_SETUP			0x09	//MMA8451 only
#define  MMA845x_TRIG_CFG			0x0A	//MMA8451 only
#define  MMA845x_SYSMOD				0x0B
#define  MMA845x_INT_SOURCE			0x0C
#define  MMA845x_WHO_AM_I			0x0D
#define  MMA845x_XYZ_DATA_CFG		0x0E
#define  MMA845x_HP_FILTER_CUTOFF	0x0F
#define  MMA845x_PL_STATUS			0x10
#define  MMA845x_PL_CFG				0x11
#define  MMA845x_PL_COUNT			0x12
#define  MMA845x_PL_BF_ZCOMP		0x13
#define  MMA845x_PL_P_L_THS_REG		0x14
#define  MMA845x_FF_MT_CFG			0x15
#define  MMA845x_FF_MT_SRC			0x16
#define  MMA845x_FF_MT_THS			0x17
#define  MMA845x_FF_MT_COUNT		0x18
#define  MMA845x_TRANSIENT_CFG		0x1D
#define  MMA845x_TRANSIENT_SRC		0x1E
#define  MMA845x_TRANSIENT_THS		0x1F
#define  MMA845x_TRANSIENT_COUNT	0x20
#define  MMA845x_PULSE_CFG			0x21
#define  MMA845x_PULSE_SRC			0x22
#define  MMA845x_PULSE_THSX			0x23
#define  MMA845x_PULSE_THSY			0x24
#define  MMA845x_PULSE_THSZ			0x25
#define  MMA845x_PULSE_TMLT			0x26
#define  MMA845x_PULSE_LTCY			0x27
#define  MMA845x_PULSE_WIND			0x28
#define  MMA845x_ASLP_COUNT			0x29
#define  MMA845x_CTRL_REG1			0x2A
#define  MMA845x_CTRL_REG2			0x2B
#define  MMA845x_CTRL_REG3			0x2C
#define  MMA845x_CTRL_REG4			0x2D
#define  MMA845x_CTRL_REG5			0x2E
#define  MMA845x_OFF_X				0x2F
#define  MMA845x_OFF_Y				0x30
#define  MMA845x_OFF_Z				0x31

uint8 mma845x_readreg(uint8 reg_add);
void mma845x_writereg(uint8 reg_add, uint8 reg_val);
void mma845x_read_data(uint8 *g_value);

#endif /* MMA845X_H_ */
