
#ifndef STEPCOUNT_H_
#define STEPCOUNT_H_

#include <project.h>

#define byte uint8
    
void startDetection();
void stopDetection();
void resetStepCount();
short processAccelarationData(short f_x_i16, short f_y_i16, short f_z_i16);
unsigned long getStepCount();
void InitAlgo();
unsigned char getActivity(void);
void enableRobustness(void);
void disableRobustness(void);

#endif /* STEPCOUNT_H_ */
