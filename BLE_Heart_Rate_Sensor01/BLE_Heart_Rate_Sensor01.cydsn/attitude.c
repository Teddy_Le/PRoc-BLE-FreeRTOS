/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */

#include <stdlib.h>

/* Scheduler include files. */
#include "FreeRTOS.h"
#include "task.h"

/* Demo program include files. */
#include "project.h"
#include "main.h"
#include "attitude.h"

Attitude_Data attitude_atri;

TaskHandle_t Rd_task;
TaskHandle_t Ff_task;
TaskHandle_t Mc_task;

void _lwevent_set(void)
{
    BaseType_t res;
    
    res = xTaskNotify(Mc_task,1,eNoAction); 
    
    return;
}

void _lwevent_wait(void)
{
    BaseType_t res;
    
    res = xTaskNotifyWait(0x00,0xFFFFFFFF,NULL,portMAX_DELAY); 
    
    return;
}

portTASK_FUNCTION( vReadDataFlashTask, pvParameters )
{
    TickType_t xLastFlashTime,delay = 5;

	/* The parameters are not used. */
	( void ) pvParameters;



	/* We need to initialise xLastFlashTime prior to the first call to 
	vTaskDelayUntil(). */
	xLastFlashTime = xTaskGetTickCount();
    
	for(;;)
	{
		/* Delay for half the flash period then turn the LED on. */
		vTaskDelayUntil( &xLastFlashTime, delay );
		RdSensData_task_runonece();
	}
} /*lint !e715 !e818 !e830 Function definition must be standard for task creation. */

portTASK_FUNCTION( vFusionFlashTask, pvParameters )
{
    TickType_t xLastFlashTime,delay = 40;
    

	/* The parameters are not used. */
	( void ) pvParameters;



	/* We need to initialise xLastFlashTime prior to the first call to 
	vTaskDelayUntil(). */
	xLastFlashTime = xTaskGetTickCount();

	for(;;)
	{
		/* Delay for half the flash period then turn the LED on. */
		vTaskDelayUntil( &xLastFlashTime, delay );
		Fusion_task_runonce();
        CreateAndSendPackets_UART(0,&attitude_atri);
//        UART_DEB_1_PutHexInt(attitude_atri.index);
//        UART_DEB_1_PutCRLF();
	}
} /*lint !e715 !e818 !e830 Function definition must be standard for task creation. */

portTASK_FUNCTION( vMagCalFlashTask, pvParameters )
{
    TickType_t xLastFlashTime,delay = 15000;

	/* The parameters are not used. */
	( void ) pvParameters;



	/* We need to initialise xLastFlashTime prior to the first call to 
	vTaskDelayUntil(). */
	xLastFlashTime = xTaskGetTickCount();

	for(;;)
	{
		/* Delay for half the flash period then turn the LED on. */
		MagCal_task_runonce();
	}
} /*lint !e715 !e818 !e830 Function definition must be standard for task creation. */

void vStartReadDataFlashTasks( UBaseType_t uxPriority )
{
	/* Create the three tasks. */
		/* Spawn the task. */
	xTaskCreate( vReadDataFlashTask, "ReadData", 100, NULL, uxPriority, &Rd_task );
}

void vStartFusionFlashTasks( UBaseType_t uxPriority )
{
	/* Create the three tasks. */
		/* Spawn the task. */
	xTaskCreate( vFusionFlashTask, "Fusion", 300, NULL, uxPriority, &Ff_task );
}

void vStartMagCalFlashTasks( UBaseType_t uxPriority )
{
	/* Create the three tasks. */
		/* Spawn the task. */
	xTaskCreate( vMagCalFlashTask, "MagCal", 200, NULL, uxPriority, &Mc_task);
}