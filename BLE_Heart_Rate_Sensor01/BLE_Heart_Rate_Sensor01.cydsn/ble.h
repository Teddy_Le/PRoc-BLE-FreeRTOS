/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
#ifndef BLE_H
#define BLE_H
    
#include "FreeRTOS.h"
#include "project.h"

typedef enum{
    CYBLE_EVT_ASIX_NOTIFICATION_ENABLED = 1,
    CYBLE_EVT_ASIX_NOTIFICATION_DISABLED,
    CYBLE_EVT_CUS_NOTIFICATION_ENABLED,
    CYBLE_EVT_CUS_NOTIFICATION_DISABLED
}BLE_EVT_T;
   
    
void CyBle_AsixWriteEventHandler(CYBLE_GATTS_WRITE_REQ_PARAM_T *eventParam);
    
void vStartBleProcessTasks( UBaseType_t uxPriority );
void vStartHrssProcessTasks( UBaseType_t uxPriority );
    
#endif