/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */

#ifndef TSTATE_H
#define TSTATE_H
    

#include "FreeRTOS.h"
    
void vStartTstateTasks( UBaseType_t uxPriority );
    
#endif
